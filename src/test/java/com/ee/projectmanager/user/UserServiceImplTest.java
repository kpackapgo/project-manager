package com.ee.projectmanager.user;

import com.ee.projectmanager.exceptionhandling.EmailAlreadyExistsException;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;


import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Captor
    ArgumentCaptor<Pageable> pageableCaptor;

    @Test
    public void userIsSaved() {
        var user = new User();
        user.setId(1L);

        when(userRepository.save(user)).thenReturn(user);

        var savedUser = userService.saveUser(user);

        verify(userRepository, times(1)).save(user);

        Assertions.assertNotNull(savedUser);
        Assertions.assertSame(user.getId(), savedUser.getId());
    }

    @Test
    public void userWithEmailThatAlreadyExistsCannotBeSaved() {
        var userId = 2L;
        var email = "random@gmail.com";
        var user = new User();
        user.setId(userId);
        user.setEmail(email);

        Assertions.assertThrows(EmailAlreadyExistsException.class, () -> {

            doThrow(new EmailAlreadyExistsException(email)).when(userRepository).save(user);

            userService.saveUser(user);
        });
    }

    @Test
    public void loadUsersWithNameContaining() {

        var text = "name";
        var user = new User();
        user.setId(1L);
        var users = Lists.newArrayList(user);

        when(userRepository.findByNameContaining(eq(text), any())).thenReturn(users);

        userService.getUsersByNameContaining(text, 3, 5);

        verify(userRepository, times(1)).findByNameContaining(eq(text), pageableCaptor.capture());

        Assertions.assertSame(3, pageableCaptor.getValue().getPageNumber());
        Assertions.assertSame(5, pageableCaptor.getValue().getPageSize());
    }

    @Test
    public void loadUsersWithEmailContaining() {
        var text = "gmail";
        var user = new User();
        user.setId(1L);
        var users = Lists.newArrayList(user);

        when(userRepository.findByEmailContaining(eq(text), any())).thenReturn(users);

        userService.getUsersByEmailContaining(text, 3, 5);

        verify(userRepository, times(1)).findByEmailContaining(eq(text), pageableCaptor.capture());

        Assertions.assertSame(3, pageableCaptor.getValue().getPageNumber());
        Assertions.assertSame(5, pageableCaptor.getValue().getPageSize());
    }

    @Test
    public void loadUsersWithNameContainingANdEmailContaining() {
        var nameText = "name";
        var emailText = "gmail";
        var user = new User();
        user.setId(1L);
        var users = Lists.newArrayList(user);

        when(userRepository.findByNameContainingAndEmailContaining(eq(nameText), eq(emailText), any())).thenReturn(users);

        userService.getUsersByNameContainingAndEmailContaining(nameText, emailText, 3, 5);

        verify(userRepository, times(1)).findByNameContainingAndEmailContaining(eq(nameText), eq(emailText), pageableCaptor.capture());

        Assertions.assertSame(3, pageableCaptor.getValue().getPageNumber());
        Assertions.assertSame(5, pageableCaptor.getValue().getPageSize());
    }
}