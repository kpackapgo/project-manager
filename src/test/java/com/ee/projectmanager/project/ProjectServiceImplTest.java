package com.ee.projectmanager.project;

import com.ee.projectmanager.exceptionhandling.ProjectNotFoundException;
import com.ee.projectmanager.exceptionhandling.UserAlreadyAssignedException;
import com.ee.projectmanager.exceptionhandling.UserNotFoundException;
import com.ee.projectmanager.user.User;
import com.ee.projectmanager.user.UserRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProjectServiceImplTest {

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private ProjectServiceImpl projectService;

    @Captor
    ArgumentCaptor<Pageable> pageableCaptor;

    @Captor
    ArgumentCaptor<Project> projectCaptor;

    @Test
    public void returnProjectByGivenId() {
        var project = new Project();
        project.setId(1L);

        when(projectRepository.findById(1L)).thenReturn(Optional.of(project));

        Project returnedProject = projectService.getProject(1L);

        verify(projectRepository, times(1)).findById(1L);

        Assertions.assertNotNull(returnedProject);
        Assertions.assertSame(project.getId(), returnedProject.getId());
    }

    @Test
    public void returnNullWhenProjectWithIdIsNotFound() {
        var project = new Project();
        project.setId(1L);

        when(projectRepository.findById(1L)).thenReturn(Optional.empty());

        var returnedProject = projectService.getProject(1L);

        verify(projectRepository, times(1)).findById(1L);

        Assertions.assertNull(returnedProject);
    }

    @Test
    public void projectIsSaved() {
        var project = new Project();
        project.setId(1L);

        when(projectRepository.save(project)).thenReturn(project);

        var savedProject = projectService.saveProject(project);

        verify(projectRepository, times(1)).save(project);

        Assertions.assertNotNull(savedProject);
        Assertions.assertSame(project.getId(), savedProject.getId());
    }

    @Test
    public void projectIsDeleted() {
        projectService.deleteProject(1L);

        verify(projectRepository, times(1)).deleteById(1L);
    }

    @Test
    public void exceptionWhenTryingToDeleteNotExistingProject() {
        Assertions.assertThrows(ProjectNotFoundException.class, () -> {

            doThrow(new ProjectNotFoundException(1L)).when(projectRepository).deleteById(1L);

            projectService.deleteProject(1L);
        });
    }

    @Test
    public void loadProjectsWithNameContaining() {
        var project = new Project();
        project.setId(1L);
        var projects = Lists.newArrayList(project);

        when(projectRepository.findByNameContaining(eq("name"), any())).thenReturn(projects);

        projectService.getProjectByNameContaining("name", 3, 5);

        verify(projectRepository, times(1)).findByNameContaining(eq("name"), pageableCaptor.capture());

        Assertions.assertSame(3, pageableCaptor.getValue().getPageNumber());
        Assertions.assertSame(5, pageableCaptor.getValue().getPageSize());
    }

    @Test
    public void userIsAssignedToProject() {
        var projectId = 1L;
        var userId = 2L;

        var project = new Project();
        project.setId(projectId);

        var user = new User();
        user.setId(userId);

        when(projectRepository.findById(projectId)).thenReturn(Optional.of(project));
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        projectService.assignUserToProject(projectId, userId);

        verify(projectRepository, times(1)).findById(projectId);
        verify(userRepository, times(1)).findById(userId);
        verify(projectRepository, times(1)).save(projectCaptor.capture());

        Assertions.assertSame(user, projectCaptor.getValue().getAssignedUsers().get(0));
    }

    @Test
    public void exceptionIsThrowIfTryingToAssignUserToAProjectThatDoesNotExists() {
        var projectId = 1L;
        var userId = 2L;

        Assertions.assertThrows(ProjectNotFoundException.class, () -> {

            doThrow(new ProjectNotFoundException(projectId)).when(projectRepository).findById(projectId);

            projectService.assignUserToProject(projectId, userId);
        });
    }

    @Test
    public void exceptionIsThrowIfTryingToAssignAUserThatDoesNotExists() {
        var projectId = 1L;
        var userId = 2L;
        Project project = new Project();
        project.setId(projectId);

        Assertions.assertThrows(UserNotFoundException.class, () -> {


            when(projectRepository.findById(projectId)).thenReturn(Optional.of(project));
            doThrow(new UserNotFoundException(userId)).when(userRepository).findById(userId);

            projectService.assignUserToProject(projectId, userId);
        });
    }

    @Test
    public void exceptionIsThrowIfTryingToAssignAUserThatIsAlreadyAssignedToTheProject() {
        var projectId = 1L;
        var userId = 2L;
        Project project = new Project();
        project.setId(projectId);
        User user = new User();
        user.setId(userId);

        project.setAssignedUsers(Lists.newArrayList(user));

        Assertions.assertThrows(UserAlreadyAssignedException.class, () -> {

            when(projectRepository.findById(projectId)).thenReturn(Optional.of(project));

            projectService.assignUserToProject(projectId, userId);
        });
    }
}