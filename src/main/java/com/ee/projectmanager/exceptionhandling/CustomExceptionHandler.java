package com.ee.projectmanager.exceptionhandling;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ProjectNotFoundException.class, UserNotFoundException.class})
    protected ResponseEntity<Object> handleMissingData(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {UserAlreadyAssignedException.class, EmailAlreadyExistsException.class})
    protected ResponseEntity<Object> handleConflictData(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
    @ExceptionHandler(value = {TransactionSystemException.class})
    protected ResponseEntity<Object> handleConstraintViolation(RuntimeException ex, WebRequest request) {
        Throwable cause = ((TransactionSystemException) ex).getRootCause();
        if (cause instanceof ConstraintViolationException) {
            Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) cause).getConstraintViolations();
            List<String> exceptionsMessages = new ArrayList<>();
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                exceptionsMessages.add("Constrain error in field: " + constraintViolation.getPropertyPath() + " | Reason: "+ constraintViolation.getMessage());
            }
            return handleExceptionInternal(ex, exceptionsMessages, new HttpHeaders(), HttpStatus.CONFLICT, request);
        }
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

}
