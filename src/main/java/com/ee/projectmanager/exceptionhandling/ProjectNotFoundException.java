package com.ee.projectmanager.exceptionhandling;

public class ProjectNotFoundException extends RuntimeException {

    private Long projectId;

    public ProjectNotFoundException(Long projectId) {

        this.projectId = projectId;
    }

    @Override
    public String getMessage() {
        return "Project with id " + projectId + " does not exists.";
    }

}
