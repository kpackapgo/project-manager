package com.ee.projectmanager.exceptionhandling;

public class UserAlreadyAssignedException extends RuntimeException {

    private Long userId;
    private Long projectId;

    public UserAlreadyAssignedException(Long userId, Long projectId) {

        this.userId = userId;
        this.projectId = projectId;
    }

    @Override
    public String getMessage() {
        return "User with id " + userId + " is already assigned to project with id "+projectId;
    }

}
