package com.ee.projectmanager.exceptionhandling;

public class EmailAlreadyExistsException extends RuntimeException {
    private String email;

    public EmailAlreadyExistsException(String email) {

        this.email = email;
    }

    @Override
    public String getMessage() {
        return "Email needs to be unique. User with email -> " + email + " <- already exists.";
    }

}
