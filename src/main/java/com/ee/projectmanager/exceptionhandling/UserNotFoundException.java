package com.ee.projectmanager.exceptionhandling;

public class UserNotFoundException extends RuntimeException{

    private Long userId;

    public UserNotFoundException(Long userId) {

        this.userId = userId;
    }

    @Override
    public String getMessage() {
        return "User with id " + userId + " does not exists.";
    }

}
