package com.ee.projectmanager.project;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("projects")
public class ProjectController {

    private ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public List<Project> getAll() {
        return projectService.getProjects();
    }

    @GetMapping("{id}")
    public Project getOne(@PathVariable  Long id) {
        return  projectService.getProject(id);
    }

    @PostMapping
    public void save(@RequestBody Project project) {
        projectService.saveProject(project);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        projectService.deleteProject(id);
    }

    @GetMapping("name-containing/{text}/{pageIndex}/{pageSize}")
    public List<Project> getAllByNameContaining(@PathVariable String text, @PathVariable int pageIndex, @PathVariable int pageSize) {
        return projectService.getProjectByNameContaining(text, pageIndex, pageSize);
    }

    @PutMapping("assign-user/{projectId}/{userId}")
    public void assignUser(@PathVariable Long projectId, @PathVariable Long userId) {
        projectService.assignUserToProject(projectId, userId);
    }
}
