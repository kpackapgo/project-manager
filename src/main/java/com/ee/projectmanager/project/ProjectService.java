package com.ee.projectmanager.project;

import java.util.List;

public interface ProjectService {

    List<Project> getProjects();

    Project saveProject(Project project);

    void deleteProject(Long projectId);

    Project getProject(Long projectId);

    List<Project> getProjectByNameContaining(String text, int pageIndex, int pageSize);

    void assignUserToProject(Long projectId, Long userId);
}
