package com.ee.projectmanager.project;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

    @Query(value = "SELECT new com.ee.projectmanager.project.Project(p.id, p.name, p.description) FROM Project p")
    List<Project> findAll();

    List<Project> findByNameContaining(String text, Pageable pageable);
}
