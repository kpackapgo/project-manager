package com.ee.projectmanager.project;

import com.ee.projectmanager.exceptionhandling.ProjectNotFoundException;
import com.ee.projectmanager.exceptionhandling.UserAlreadyAssignedException;
import com.ee.projectmanager.exceptionhandling.UserNotFoundException;
import com.ee.projectmanager.user.User;
import com.ee.projectmanager.user.UserRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository;

    private UserRepository userRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository, UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    /**
     * Get all projects from the database
     * @return list with all projects or an empty list if no projects
     */
    @Override
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

    /**
     * Get a single project from the database
     * @param projectId the id of the projects that is going to be fetched from the database
     * @return the projects corresponding to the given projectId or null if no project
     */
    @Override
    public Project getProject(Long projectId) {
        return projectRepository.findById(projectId).orElse(null);
    }

    /**
     * Save or update a projects to the database
     * @param project the project that is going to be persisted
     * @return the persisted project
     */
    @Override
    public Project saveProject(Project project) {
        return projectRepository.save(project);
    }

    /**
     * Delete a specific project from the database
     * @param projectId the id of the projects that is going to be deleted
     */
    @Override
    public void deleteProject(Long projectId) {
        try {
            projectRepository.deleteById(projectId);
        } catch (EmptyResultDataAccessException e) {
            throw new ProjectNotFoundException(projectId);
        }
    }

    /**
     * Get all projects containing a given text in their name
     * @param text the text for which projects are going to be matched in their name
     *
     * @return list of the matching projects or empty list if no matching projects
     */
    @Override
    public List<Project> getProjectByNameContaining(String text, int pageIndex, int pageSize) {
        return projectRepository.findByNameContaining(text, PageRequest.of(pageIndex, pageSize));
    }

    /**
     * Assign a user to a project
     * @param projectId the id of the project to which the user is going to be assigned
     * @param userId the id of the user who is going to be assigned
     */
    @Override
    public void assignUserToProject(Long projectId, Long userId) {
        Project project = projectRepository.findById(projectId).orElseThrow(() -> new ProjectNotFoundException(projectId));

        List<User> assignedUsers = project.getAssignedUsers();

        if(assignedUsers == null) {
            assignedUsers = new ArrayList<>();
        }
        assignedUsers.forEach(user -> {if(user.getId() == userId) throw new UserAlreadyAssignedException(userId, projectId);});

        User userToBeAssigned = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

        assignedUsers.add(userToBeAssigned);

        project.setAssignedUsers(assignedUsers);

        projectRepository.save(project);
    }
}
