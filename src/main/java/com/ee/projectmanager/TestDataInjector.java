package com.ee.projectmanager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TestDataInjector {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {

        Project project = new Project();
        project.setName("Project one");
        project.setDescription("Description is here");

        User user1 = new User();
        user1.setName("Krasi");
        user1.setEmail("kdkd@abv.bg");

        List<User> users = new ArrayList<>();
        user1 = userRepository.save(user1);
        users.add(user1);

        project.setAssignedUsers(users);

        projectRepository.save(project);
    }
}
