package com.ee.projectmanager.user;


import com.ee.projectmanager.exceptionhandling.EmailAlreadyExistsException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Get all users
     * @return list of all users from the database or an empty list if no users
     */
    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    /**
     * Saves or updates an exissting user to the database
     * @param user the object that is going to be persisted
     * @return the persisted object
     */
    @Override
    public User saveUser(User user) {
        try {
            return userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new EmailAlreadyExistsException(user.getEmail());
        }
    }

    /**
     * Delete a specified user from the database
     * @param userId the id of the user that is going to be deleted
     */
    @Override
    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    /**
     * Get all users containing a given text in their name
     * @param text the text for which users are going to be matched in their name
     * @return list of the matching users or empty list if no matching users
     */
    @Override
    public List<User> getUsersByNameContaining(String text, int pageIndex, int pageSize) {
        return userRepository.findByNameContaining(text, PageRequest.of(pageIndex, pageSize));
    }

    /**
     * Get all users containing a given text in their email
     * @param text the text for which users are going to be matched in their email
     * @return list of the matching users or empty list if no matching users
     */
    @Override
    public List<User> getUsersByEmailContaining(String text, int pageIndex, int pageSize) {
        return userRepository.findByEmailContaining(text, PageRequest.of(pageIndex, pageSize));
    }

    /**
     * Get all users containing a given text in their name nad in their email
     * @param nameText the text for which the users are going to be matched in their name
     * @param emailText the text for which the users are going to be matched in their email
     * @return list of the matching users or empty list if no matching users
     */
    @Override
    public List<User> getUsersByNameContainingAndEmailContaining(String nameText, String emailText, int pageIndex, int pageSize) {
        return userRepository.findByNameContainingAndEmailContaining(nameText, emailText, PageRequest.of(pageIndex, pageSize));
    }
}
