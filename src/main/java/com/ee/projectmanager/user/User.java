package com.ee.projectmanager.user;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    @NotBlank
    @Column(length = 25)
    @Size(min = 1, max = 25)
    private String name;

    @NotNull
    @Column(length = 100, unique = true)
    @Size(max = 100)
    @Email
    private String email;
}
