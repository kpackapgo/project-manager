package com.ee.projectmanager.user;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getUsers();
    }

    @PostMapping
    public void save(@RequestBody User user) {
        userService.saveUser(user);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @GetMapping("name-containing/{text}/{pageIndex}/{pageSize}")
    public List<User> getAllByNameContaining(@PathVariable String text, @PathVariable int pageIndex, @PathVariable int pageSize) {
        return userService.getUsersByNameContaining(text, pageIndex, pageSize);
    }

    @GetMapping("email-containing/{text}/{pageIndex}/{pageSize}")
    public List<User> getAllByEmailContaining(@PathVariable String text, @PathVariable int pageIndex, @PathVariable int pageSize) {
        return userService.getUsersByEmailContaining(text, pageIndex, pageSize);
    }

    @GetMapping("name-email-containing/{nameText}/{emailText}/{pageIndex}/{pageSize}")
    public List<User> getAllByNameContainingAndEmailContaining(@PathVariable String nameText, @PathVariable String emailText,  @PathVariable int pageIndex, @PathVariable int pageSize) {
        return userService.getUsersByNameContainingAndEmailContaining(nameText, emailText, pageIndex, pageSize);
    }
}
