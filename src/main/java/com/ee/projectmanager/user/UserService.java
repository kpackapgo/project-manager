package com.ee.projectmanager.user;

import java.util.List;

public interface UserService {

    List<User> getUsers();

    User saveUser(User user);

    void deleteUser(Long userId);

    List<User> getUsersByNameContaining(String text, int pageIndex, int pageSize);

    List<User> getUsersByEmailContaining(String text, int pageIndex, int pageSize);

    List<User> getUsersByNameContainingAndEmailContaining(String nameText, String emailText, int pageIndex, int pageSize);
}
