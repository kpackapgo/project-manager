package com.ee.projectmanager.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    List<User> findAll();

    List<User> findByNameContaining(String text, Pageable pageable);

    List<User> findByEmailContaining(String text, Pageable pageable);

    List<User> findByNameContainingAndEmailContaining(String nameText, String emailText, Pageable pageable);
}
